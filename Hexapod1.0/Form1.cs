﻿using System;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using System.IO;


namespace Hexapod
{
    public partial class Form1 : Form
    {
        private Joystick joystick;
        private bool[] joystickButtons;

        Leg one = new Leg()
        {
            shangle = 0,
            s = 1
        };


        Leg R1 = new Leg
        {
            ch1 = 16,
            ch2 = 17,
            ch3 = 18,
            // НАДО Настроить
            kmin1 = 600,
            kmax1 = 2450,
            kmin2 = 600,
            kmax2 = 1900,
            kmin3 = 1050,
            kmax3 = 2400,

            k001 = 1500,
            k901 = 2379,
            k002 = 600,
            k902 = 1500,
            k003 = 2368,
            k903 = 1500,

            shangle = 60,
            s = 1
        };

        Leg R2 = new Leg
        {
            ch1 = 20,
            ch2 = 21,
            ch3 = 22,
            //НаДО откалибровать
            kmin1 = 600,
            kmax1 = 2450,
            kmin2 = 600,
            kmax2 = 1900,
            kmin3 = 1050,
            kmax3 = 2400,

            k001 = 1500,
            k901 = 2379,
            k002 = 600,
            k902 = 1500,
            k003 = 2368,
            k903 = 1500,

            shangle = 0,
            s = 1
        };

        Leg R3 = new Leg
        {
            ch1 = 24,
            ch2 = 25,
            ch3 = 26,

            kmin1 = 600,
            kmax1 = 2450,
            kmin2 = 600,
            kmax2 = 1900,
            kmin3 = 1050,
            kmax3 = 2400,

            k001 = 1500,
            k901 = 2379,
            k002 = 600,
            k902 = 1500,
            k003 = 2368,
            k903 = 1500,

            shangle = -60,
            s = 1
        };

        Leg L1 = new Leg
        {
            ch1 = 0,
            ch2 = 1,
            ch3 = 2,

            kmin1 = 600,
            kmax1 = 2400,
            kmin2 = 1040,
            kmax2 = 2500,
            kmin3 = 1050,
            kmax3 = 1930,

            k001 = 1500,
            k901 = 2392,
            k002 = 2467,
            k902 = 1500,
            k003 = 600,
            k903 = 1500,

            shangle = 120,
            s = -1
        };

        Leg L2 = new Leg
        {
            ch1 = 4,
            ch2 = 5,
            ch3 = 6,

            kmin1 = 600,
            kmax1 = 2400,
            kmin2 = 1040,
            kmax2 = 2500,
            kmin3 = 1050,
            kmax3 = 1930,

            k001 = 1500,
            k901 = 2377,
            k002 = 2467,
            k902 = 1500,
            k003 = 600,
            k903 = 1500,

            shangle = 180,
            s = -1
        };

        Leg L3 = new Leg
        {
            ch1 = 8,
            ch2 = 9,
            ch3 = 10,

            kmin1 = 600,
            kmax1 = 2400,
            kmin2 = 1040,
            kmax2 = 2500,
            kmin3 = 1050,
            kmax3 = 1930,

            k001 = 1500,
            k901 = 2384,
            k002 = 2467,
            k902 = 1500,
            k003 = 600,
            k903 = 1500,

            shangle = -120,

            s = -1
        };

        Leg[] legs = new Leg[6];

        //legs[0] = one;

        

        public bool comswitch = false;

        public System.IO.Ports.SerialPort sport;

        public Form1()
        {
            InitializeComponent();
            legs[0] = R1;
            legs[1] = R2;
            legs[2] = R3;
            legs[3] = L1;
            legs[4] = L2;
            legs[5] = L3;
        }

        private bool connectToJoystick(Joystick joystick)
        {
            for (int i = 0; i < 10; i++)
            {
                string sticks = joystick.FindJoysticks();
                if (sticks != null)
                {
                    if (joystick.AcquireJoystick(sticks))
                    {
                        enableTimer();
                        return true;
                    }
                }
            }
            return false;
        }

        private void enableTimer()
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new ThreadStart(delegate ()
                {
                    timer1.Enabled = true;
                }));
            }
            else
                timer1.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Fundament.TIME = Decimal.ToInt16(numericUpDown9.Value);
            try
            {
                //sport.Write(Leg.Cmd(Decimal.ToInt32(numericUpDown1.Value), Decimal.ToInt32(numericUpDown2.Value), Decimal.ToInt32(numericUpDown3.Value)) + "\r");
                string cmd = L2.Point(Decimal.ToDouble(numericUpDown1.Value),
                                      Decimal.ToDouble(numericUpDown2.Value),
                                      Decimal.ToDouble(numericUpDown3.Value),
                                      Decimal.ToDouble(numericUpDown4.Value));
                //string cmd = R2.Point(Decimal.ToDouble(numericUpDown1.Value), Decimal.ToDouble(numericUpDown2.Value), Decimal.ToDouble(numericUpDown3.Value));
                toolStripStatusLabel1.Text = cmd;
                
                sport.Write(cmd + "\r");
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

            Fundament.TIME = Decimal.ToInt16(numericUpDown9.Value);
            try
            {
                //string cmd = one.Point(Decimal.ToDouble(numericUpDown1.Value), Decimal.ToDouble(numericUpDown2.Value), 1500);
                string cmd = Leg.Cmd(L2.ch1, 1500, Fundament.TIME) + Leg.Cmd(L2.ch2, 1500, Fundament.TIME) + Leg.Cmd(L2.ch3, 1500, Fundament.TIME);
                toolStripStatusLabel1.Text = cmd;
                sport.Write(cmd + "\r");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (comswitch == false)
            {
                sport = new System.IO.Ports.SerialPort(textBox1.Text,
                                                       115200,
                                                       System.IO.Ports.Parity.None,
                                                       8,
                                                       System.IO.Ports.StopBits.One);
                try
                {
                    sport.Open();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

                button3.Text = "Отключить";
                comswitch = true;
            } else
            {
                sport.Close();

                button3.Text = "Подключить";
                comswitch = false;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Fundament.TIME = Decimal.ToInt16(numericUpDown9.Value);
            try
            {
                L2.Step(sport,
                    Decimal.ToDouble(numericUpDown1.Value),
                    Decimal.ToDouble(numericUpDown5.Value),
                    Decimal.ToDouble(numericUpDown6.Value),
                    Decimal.ToDouble(numericUpDown7.Value),
                    Decimal.ToDouble(numericUpDown8.Value)
                );
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            char key = e.KeyChar;
            
            Fundament.TIME = Decimal.ToInt16(numericUpDown9.Value);

            try
            {

                switch (key)
                {
                    case 'w':
                        label10.Text = "↑";
                        Movement.Line(sport, Decimal.ToDouble(numericUpDown1.Value), Decimal.ToDouble(numericUpDown6.Value), Decimal.ToDouble(numericUpDown7.Value), Decimal.ToDouble(numericUpDown8.Value), legs);
                        break;
                    case 's':
                        label10.Text = "↓";
                        Movement.Line(sport, Decimal.ToDouble(numericUpDown1.Value), Decimal.ToDouble(numericUpDown6.Value), Decimal.ToDouble(numericUpDown7.Value), 180, legs);
                        break;
                    case 'd':
                        label10.Text = "→";
                        Movement.Line(sport, Decimal.ToDouble(numericUpDown1.Value), Decimal.ToDouble(numericUpDown6.Value), Decimal.ToDouble(numericUpDown7.Value), 90, legs);
                        break;
                    case 'a':
                        label10.Text = "←";
                        Movement.Line(sport, Decimal.ToDouble(numericUpDown1.Value), Decimal.ToDouble(numericUpDown6.Value), Decimal.ToDouble(numericUpDown7.Value), 270, legs);
                        break;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            try
            {
                joystick.UpdateStatus();
                joystickButtons = joystick.buttons;


                if (joystick.Xaxis != 32768 && joystick.Yaxis != 32768)
                {
                    double dir = Math.Atan(joystick.Yaxis / joystick.Xaxis) / Math.PI * 180;
                    Movement.Line(sport, Decimal.ToDouble(numericUpDown1.Value), Decimal.ToDouble(numericUpDown6.Value), Decimal.ToDouble(numericUpDown7.Value), dir, legs);
                    
                }
                    

                /*
                for (int i = 0; i < joystickButtons.Length; i++)
                {
                    if (joystickButtons[i] == true)
                        output.Text += "Button " + i + " Pressed\n";
                }*/
            }
            catch
            {
                timer1.Enabled = false;
                connectToJoystick(joystick);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                string path = Directory.GetCurrentDirectory();
                string[] dirs = Directory.GetFiles(path + @"\gait\", "*.gi");
                
                for (int i = 0; i < dirs.Length; i++)
                {
                    dirs[i] = dirs[i].Split('\\').Last();
                }
                Console.WriteLine("The number of files starting with c is {0}.", dirs.Length);
                comboBox1.Items.Clear();
                comboBox1.Items.AddRange(dirs);
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Fundament.ChangeGait(comboBox1.SelectedItem.ToString());
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                Movement.Jump(sport, Decimal.ToDouble(numericUpDown1.Value), Decimal.ToDouble(numericUpDown3.Value), Decimal.ToInt16(numericUpDown9.Value), legs);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            //кнопка подключения джойстика
            try
            {
                joystick = new Joystick(this.Handle);
               /* if (connectToJoystick(joystick))
                {
                    button7.Text = "Джойстик подключен";
                }
                else {
                    button7.Text = "Подключить джойстик";
                    MessageBox.Show("Не удалось подключить джойстик");
                };*/
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
