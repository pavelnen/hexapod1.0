﻿using System;
using System.Collections;
using System.Linq;
using System.Windows.Forms;
using System.Runtime.Serialization;
using System.IO;

namespace Hexapod
{
    public class Fundament
    {
        public static short TIME = 1000;
        //public const double HIP = 5.5; //Бедро
        //public const double SHIN = 13; //Голень
        public const double HIP = 5.5; //Бедро
        public const double SHIN = 14; //Голень
        public const double SHIFT = 3.5; //Вынос бедра из сустава

        public static short metronom = 0;
        public static bool tick = false;

        //public static int[,] gait = { { 1, 0, 1, 0, 1, 0 }, { 0, 1, 0, 1, 0, 1 } };
        public static ArrayList gait = new ArrayList();

        public static short takts = 2;

        public static void WaitStop(System.IO.Ports.SerialPort sport)
        {
            sport.Write("Q\r");
            while (sport.ReadChar() == '+')
            {

                //System.Threading.Thread.Sleep(20);
                /*if (tick)
                {
                    while (tick) { }
                } else if (!tick)
                {
                    while (!tick) { }
                }*/
                sport.Write("Q\r");
            }
        }

        public static void ChangeGait(string filename)
        {
            string path = Directory.GetCurrentDirectory();
            StreamReader objReader = new StreamReader(path + @"\gait\" + filename);
            int[] states = new int[6];
            string sLine = ""; //ArrayList gait = new ArrayList();



            sLine = objReader.ReadLine();

            while (sLine != null)
            {
                sLine = objReader.ReadLine();
                if (sLine != null)
                {
                    gait.Add(sLine.Split('\t').Select(st => Convert.ToInt32(st)).ToArray());
                }
                    
            }
            objReader.Close();


        }


    }

    

    public class WrongCoordException : ApplicationException
    {
        public WrongCoordException() { }

        public WrongCoordException(string message) : base(message) { }

        public WrongCoordException(string message, Exception inner) : base(message, inner) { }

        protected WrongCoordException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }

    public class Leg
    {
        public short ch1, ch2, ch3;
        public short kmin1, kmax1, kmin2, kmax2, kmin3, kmax3;
        public short k001, k901, k002, k902, k003, k903;
        public double shangle;
        public short s;

        public Leg()
        {
            ch1 = 5; ch2 = 3; ch3 = 0;
            kmin1 = 600; kmax1 = 2400;
            kmin2 = 1050; kmax2 = 2400;
            kmin3 = 700; kmax3 = 2400;

            k001 = 1500; k901 = 2400;
            k002 = 1500; k902 = 2400;
            k003 = 2400; k903 = 1500;

            shangle = Math.PI / 3;

            
        }

        private static double Krad (short k00, short k90)
        {
            //Определение P/rad для сервоприводов
            return (k90 - k00) / (Math.PI / 2);
        }

        public static string Cmd(int ch, int p, int t)
        {
            //Формирование команды без завершающего CR
            string cmd = "#" + ch + "P" + p + "T" + t;
            return cmd;
        }

        private static int Ang2pos (double ang, short kst, short k00, short k90, short kmin, short kmax)
        {
            int p = kst + Convert.ToInt32( ang * Krad(k00, k90));
            if (p < kmin) { p = kmin; }
            if ( p > kmax ) { p = kmax; }
            return p;
        }
        
        private static bool ValidCoord (double hpelvis, double hfoot, double dist, double along)
        {
            double l0 = Fundament.SHIFT;
            double l1 = Fundament.HIP;
            double l2 = Fundament.SHIN;

            bool e = true;

            double vmin = Math.Abs(l1 - l2);
            double vmax = l1 + l2;
            double x = Math.Sqrt(Math.Pow((hpelvis - hfoot), 2) + Math.Pow(dist - l0,2) + Math.Pow(along,2));

            if (x.CompareTo(vmin) < 0 || x.CompareTo(vmax) > 0) {
                e = false;
            }

            return e;
        }
        
        public string Point (double hpelvis, double hfoot, double dist, double along, short time = 1000)
        {
            //hplevis - высота корпуса над полом; hfoot - высота стопы над полом;
            //dist - расстояние от нуля до стопы поперёк робота; along - рассотяние от нуля до стопы вдоль робота ;
            //shangle - поправка нулевой оси;

            //double salong = s == -1 ? -along : along;

            if (ValidCoord(hpelvis, hfoot, dist, along) == false)
            {
                throw new WrongCoordException("Невозможные координаты hpelvis = " + hpelvis + " hfoot = "
                                                + hfoot + " dist = " +  dist + " along = " + along);
            }
            //Конвертирование линейных координат в углы в узлах
            double l0 = Fundament.SHIFT;
            double l1 = Fundament.HIP;
            double l2 = Fundament.SHIN;
            
            double lpro = Math.Sqrt(Math.Pow(dist - l0, 2) + Math.Pow(along, 2)); //Длина проекции ноги на пол

            double xsqr = Math.Pow((hpelvis - hfoot), 2) + Math.Pow(lpro, 2); //Это квадрат длины!
            double x = Math.Sqrt(xsqr);

            double b = Math.Acos((l1 * l1 + l2 * l2 - xsqr) / (2 * l1 * l2)); //Угол между бедром и голенью

            double siny = l2 * Math.Sin(b) / x;
            double y = Math.Acos((l1 * l1 + xsqr - l2 * l2) / (2 * l1 * x));

            double a;
            if (hpelvis == hfoot)
            {
                a = Math.PI/2 - y;
            }
            else if (hpelvis > hfoot)
            {
                a = Math.PI - y - Math.Atan(lpro / (hpelvis - hfoot)); //Угол бедра относительно вертикальной оси
            }else
            {
                a = Math.PI / 2 - y - Math.Atan((hfoot - hpelvis) / lpro); //Угол бедра относительно вертикальной оси
            }

            double c = Math.Atan(along / dist);

            int p1 = Ang2pos(c, k001, k001, k901, kmin1, kmax1);
            if (p1 < kmin1) { p1 = kmin1; }
            if (p1 > kmax1) { p1 = kmax1; }
            int p2 = Ang2pos(a, k002, k002, k902, kmin2, kmax2);
            if (p2 < kmin2) { p2 = kmin2; }
            if (p2 > kmax2) { p2 = kmax2; }
            int p3 = Ang2pos(Math.PI - b, k003, k003, k903, kmin3, kmax3);
            if (p3 < kmin3) { p3 = kmin3; }
            if (p3 > kmax3) { p3 = kmax3; }

            string order = Cmd(ch1, p1, time) + Cmd(ch2, p2, time) + Cmd(ch3, p3, time) + "\r";
            //string order = "a = ";
            //order += a/Math.PI*180;
            //order += ", b = ";order += b / Math.PI * 180; order += ", y = ";order += y / Math.PI * 180;
            return order;

        }

        public void Step (System.IO.Ports.SerialPort sport, double hpelvis, double dist, double along, double l, double a)
        {
            // Метод, который делает шаг, начиная с определённой координаты, вдоль вектора, заданного длиной и углом (deg) поворота относительно главной оси робота

            a += shangle;

            double findist = dist + l * Math.Sin(a * Math.PI / 180);
            double finalong = along + l * Math.Cos(a * Math.PI / 180);

            double middist = dist + l / 2 * Math.Sin(a * Math.PI / 180);
            double midalong = along + l / 2 * Math.Cos(a * Math.PI / 180);

            string cmd = Point(hpelvis, 2, dist, along); //Устанавливаем ногу над точкой начала шага
            sport.Write(cmd + "\r");
            //System.Threading.Thread.Sleep(Fundament.TIME);
            Fundament.WaitStop(sport);

            cmd = Point(hpelvis, 0, dist, along); //Опускаем ногу
            sport.Write(cmd + "\r");
            //System.Threading.Thread.Sleep(Fundament.TIME);
            Fundament.WaitStop(sport);

            cmd = Point(hpelvis, 0, middist, midalong); // Промежуточное движение, чтобы нога двигалась в достаточной степени по прямой
            sport.Write(cmd + "\r");
            //System.Threading.Thread.Sleep(Fundament.TIME);
            Fundament.WaitStop(sport);

            // Возможно надо будет добавить больше промежуточных движений для большей точности
            cmd = Point(hpelvis, 0, findist, finalong); // Конечное положение
            sport.Write(cmd + "\r");
            
        }

        public string DolyaStep (System.IO.Ports.SerialPort sport, short dolya, double hpelvis, double dist, double along, double l, double a, string mode = "onedirect")
        {
            // Метод поделённый на доли такта
            // Метод, который делает шаг, начиная с определённой координаты, вдоль вектора, заданного длиной и углом (deg) поворота относительно главной оси робота

            //if (mode == "onedirect")
            //{
                a = a + shangle;
            //}
            

            if (dolya == 1) {
                return Point(hpelvis, 2, dist, along, Fundament.TIME); //Устанавливаем ногу над точкой начала шага
            } else if (dolya == 2)
            {
                return Point(hpelvis, 0, dist, along, Fundament.TIME); //Опускаем ногу
            } else if (dolya == 3)
            {
                double middist = dist + l / 2 * Math.Sin(a * Math.PI / 180);
                double midalong = along + l / 2 * Math.Cos(a * Math.PI / 180);
                return Point(hpelvis, 0, middist, midalong, Fundament.TIME); // Промежуточное движение, чтобы нога двигалась в достаточной степени по прямой
            } else if (dolya == 4)
            {
                double findist = dist + l * Math.Sin(a * Math.PI / 180);
                double finalong = along + l * Math.Cos(a * Math.PI / 180);
                return Point(hpelvis, 0, findist, finalong, Fundament.TIME); // Конечное положение
            }

            return "";
        }
    }

    public class Movement
    {

        //static int[,] gait = { { 1, 0, 1, 0, 1, 0 }, { 0, 1, 0, 1, 0, 1 } };
        //static int[,] gait = { { 0, 1, 1, 0, 1, 1 }, { 1, 0, 1, 1, 0, 1 }, { 1, 1, 0, 1, 1, 0 } };
        //static int[,] gait = { { 0, 1, 1, 1, 1, 1 }, { 1, 0, 1, 1, 1, 1 }, { 1, 1, 0, 1, 1, 1 }, { 1, 1, 1, 0, 1, 1 }, { 1, 1, 1, 1, 0, 1 }, { 1, 1, 1, 1, 1, 0 } };

        public Movement()
        {
            //gait[0,0] = { { 1, 0, 1, 0, 1, 0 }, { 0, 1, 0, 1, 0, 1 } }
        }

        public static void Line (System.IO.Ports.SerialPort sport, double hpelvis, double startstep, double lenstep, double dir, Leg[] legs )
        {
            //double dist = Math.Sqrt(Math.Pow(hpelvis, 2) + Math.Pow(Fundament.SHIN - Fundament.HIP,2));
            //bool b = dist.CompareTo(Fundament.HIP) == 1 ? true : false;

            foreach (int[] obj in Fundament.gait)
            {
                double startdist = startstep + lenstep * Math.Sin(dir * Math.PI / 180) / 2;
                double startalong = lenstep * Math.Cos(dir * Math.PI / 180) / 2;

                for (short dolya = 1; dolya <= 4; dolya++)
                {
                    string cmd = "";
                    for (int i = 0; i < 6; i++)
                    {
                    
                        if (obj[i] == 1)
                        {
                            cmd += legs[i].DolyaStep(sport, dolya, hpelvis, startdist, startalong, lenstep, dir + 180, "onedirect");
                        }
                        else if (dolya == 3)
                        {
                            cmd += legs[i].Point(hpelvis, 2, startdist, startalong);
                            
                            //System.Threading.Thread.Sleep(Fundament.TIME);
                        }
                    }
                    sport.Write(cmd + "\r");
                    Fundament.WaitStop(sport);
                }

                
                //Fundament.WaitStop(sport);
                
            }
            
        }

        public static void TurnAround(System.IO.Ports.SerialPort sport, double hpelvis, double startstep, double lenstep, string side, Leg[] legs)
        {

            double dir = side == "right" ? 90 : -90;


            foreach (int[] obj in Fundament.gait)
            {
                double startdist = startstep + lenstep * Math.Sin(dir * Math.PI / 180) / 2;
                double startalong = lenstep * Math.Cos(dir * Math.PI / 180) / 2;

                for (short dolya = 1; dolya <= 4; dolya++)
                {
                    string cmd = "";
                    for (int i = 0; i < 6; i++)
                    {

                        if (obj[i] == 1)
                        {
                            cmd += legs[i].DolyaStep(sport, dolya, hpelvis, startdist, startalong, lenstep, dir + 180, "rotate");
                        }
                        else if (dolya == 3)
                        {
                            cmd += legs[i].Point(hpelvis, 5, startdist, startalong);

                            //System.Threading.Thread.Sleep(Fundament.TIME);
                        }
                    }
                    sport.Write(cmd + "\r");
                    Fundament.WaitStop(sport);
                }
            }
        }

        public static void Jump(System.IO.Ports.SerialPort sport, double hpelvis, double dist, short time, Leg[] legs)
        {
            string cmd = "";
            for (int i = 0; i < 6; i++)
            {
                sport.Write(legs[i].Point(10, 1, dist, 0) + "\r");
                Fundament.WaitStop(sport);
                sport.Write(legs[i].Point(10, 0, dist, 0) + "\r");
                Fundament.WaitStop(sport);
            }
            
            cmd = "";
            for (int i = 0; i < 6; i++)
            {
                cmd += legs[i].Point(hpelvis, 0, dist, 0, time);
            }
            sport.Write(cmd + "\r");
            Fundament.WaitStop(sport);
            /*
            cmd = "";
            for (int i = 0; i < 6; i++)
            {
                cmd += legs[i].Point(10, 0, dist, 0, time);
            }
            sport.Write(cmd + "\r");
            Fundament.WaitStop(sport);
            */
        }
    }

    
    

    static class Program
    {
        
        
        
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
